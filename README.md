[![Codacy Badge](https://app.codacy.com/project/badge/Grade/d91055ebd83345629824ba9856a7e730)](https://app.codacy.com/gh/PieresAgustin/Tienda-Web-Angular/dashboard?utm_source=gh&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![codecov](https://codecov.io/gh/PieresAgustin/Tienda-Web-Angular/graph/badge.svg?token=WDJ9VUS8AQ)](https://codecov.io/gh/PieresAgustin/Tienda-Web-Angular)

Deployment de la app de Angular: https://solosports-tecweb.netlify.app/#/products

Deployment de la app de NestJS: https://tienda-web-gc9d.onrender.com

# Tienda-Web

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
